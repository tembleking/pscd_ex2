package main

import (
	"fmt"
	"github.com/pspaces/gospace"
	"math/rand"
	"sync"
	"time"
)

var space gospace.Space

func main() {
	rand.Seed(time.Now().UnixNano())

	space = gospace.NewSpace("linda")
	matrix := generateMatrix()

	wg := sync.WaitGroup{}
	for i := 0; i < 4; i++ {
		wg.Add(1)
		go func(id int) {
			defer wg.Done()
			process(id, matrix)
		}(i)
	}
	wg.Wait()
}

func process(id int, matrix [][]int) {
	if id == 0 {
		space.Put("rowToCheck", 99)
		space.Put("max", matrix[0][0])
	}
	for {
		var rowNum int
		t, _ := space.Get("rowToCheck", &rowNum)
		rowNum = t.GetFieldAt(1).(int)
		space.Put("rowToCheck", rowNum-1)

		if rowNum < 0 {
			if id > 0 {
				space.Put("finished", id)
			}
			break
		}

		// Random sleep to ensure that a process has to invest time to calculate the local max
		// Some processes must be faster than others at some point
		time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)

		localMax := matrix[rowNum][0]
		for num := range matrix[rowNum] {
			if num > localMax {
				localMax = num
			}
		}

		var globalMax int
		t, _ = space.Get("max", &globalMax)
		globalMax = t.GetFieldAt(1).(int)

		if localMax > globalMax {
			globalMax = localMax
		}
		fmt.Println("Proceso", id, "calculando fila", rowNum, "con máximo local", localMax, "y maximo global:", globalMax)
		space.Put("max", globalMax)
	}
	if id == 0 {
		for i := 1; i < 4; i++ {
			space.Get("finished", i)
		}

		var globalMax int
		t, _ := space.Get("max", &globalMax)
		globalMax = t.GetFieldAt(1).(int)

		fmt.Println("Max global encontrado:", globalMax)
	}
}

func generateMatrix() [][]int {
	matrix := [][]int{}
	for i := 0; i < 100; i++ {
		row := []int{}
		for j := 0; j < 100; j++ {
			row = append(row, rand.Int())
		}
		matrix = append(matrix, row)
	}
	return matrix
}
